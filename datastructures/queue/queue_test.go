package queue

import "testing"

func TestQueue(t *testing.T) {

	// Init queue
	q := &Queue{}

	// Enqueue 5 elements into the queue.
	i := 0
	for ; i < 5; i++ {
		q.Enqueue(i)
	}
	// check the size and that it is not empty.
	checkSize(t, q, i)
	if q.Empty() {
		t.Errorf("Empty(): expected: false got: true")
	}

	// Get the front element and verify it is as expected.
	e := q.Front()
	v := e.Value.(int)
	if v != 0 {
		t.Errorf("Front(): expected: 0 got %v", v)
	}
	checkSize(t, q, i)

	// Dequeue an element and verify it is as expected.
	e = q.Dequeue()
	v = e.Value.(int)
	if v != 0 {
		t.Errorf("Dequeue(): expected: 0 got: %v", v)
	}
	checkSize(t, q, i-1)

	// Dequeue all the elements and verify that the queue is empty.
	for j := 0; j < i-1; j++ {
		q.Dequeue()
	}
	checkSize(t, q, 0)
	if !q.Empty() {
		t.Errorf("Empty(): expected: true got: false")
	}
}

func checkSize(t *testing.T, q *Queue, expected int) {
	if q.Size() != expected {
		t.Errorf("Size(): expected: %v got: %v", expected, q.Size())
	}
}
