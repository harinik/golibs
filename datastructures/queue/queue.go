// Package queue is a simple implementation for a queue.
package queue

import (
	"fmt"
	"sync"
)

// Element represents an element in a queue.
type Element struct {
	Value interface{}
	next  *Element
}

// Queue data structure.
type Queue struct {
	front, back *Element
	size        int
	mx          sync.Mutex
}

// Enqueue enqueues a value to the end of the queue.
func (q *Queue) Enqueue(val interface{}) *Element {
	e := &Element{
		Value: val,
	}
	q.mx.Lock()
	defer q.mx.Unlock()
	if q.front == nil && q.back == nil {
		q.front = e
		q.back = e
	} else {
		q.back.next = e
		q.back = e
	}
	q.size++
	return e
}

// Dequeue removes an element from the front of the queue.
func (q *Queue) Dequeue() *Element {
	var e *Element
	q.mx.Lock()
	defer q.mx.Unlock()
	if q.front != nil {
		e = q.front
		q.front = q.front.next
		if q.front == nil {
			q.back = nil
		}
		q.size--
	}
	return e
}

// Front returns the element in the front of the queue without removing it.
func (q *Queue) Front() *Element {
	q.mx.Lock()
	defer q.mx.Unlock()
	return q.front
}

// Size returns the size of the queue.
func (q *Queue) Size() int {
	q.mx.Lock()
	defer q.mx.Unlock()
	return q.size
}

// Empty returns true if the queue is empty, false otherwise.
func (q *Queue) Empty() bool {
	q.mx.Lock()
	defer q.mx.Unlock()
	return q.front == nil
}

// Print prints the contents of the queue.
func (q *Queue) Print() {
	q.mx.Lock()
	defer q.mx.Unlock()
	for i := q.Front(); i != nil; i = i.next {
		fmt.Printf("%v, ", i.Value)
	}
	fmt.Println()
}
