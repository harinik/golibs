package stack

import (
	"testing"
)

func TestIntStack(t *testing.T) {
	// Init stack
	s := &Stack{}

	// Push 5 elements into the stack and check the state.
	i := 0
	for ; i < 5; i++ {
		s.Push(i)
	}
	checkSize(t, s, i)

	// Peek at the top element and verify it is as expected.
	e := s.Peek()
	got := e.Value.(int)
	if got != i-1 {
		t.Errorf("Peek(): expected: %v, got: %v", i-1, got)
	}
	checkSize(t, s, i)

	// Pop the top element and verify it is as expected.
	e = s.Pop()
	got = e.Value.(int)
	if got != i-1 {
		t.Errorf("Pop(): expected: %v, got: %v", i-1, got)
	}
	checkSize(t, s, i-1)

	// Pop all the elements and verify that the stack is empty.
	for j := 0; j < i-1; j++ {
		s.Pop()
	}
	checkSize(t, s, 0)
	if !s.Empty() {
		t.Errorf("Empty(): expected true, got false")
	}
}

func checkSize(t *testing.T, s *Stack, expected int) {
	sz := s.Size()
	if sz != expected {
		t.Errorf("Size(): expected: %v, got: %v", expected, sz)
	}
}
