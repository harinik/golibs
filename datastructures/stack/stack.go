// Package stack implements a simple stack datastructure.
package stack

import (
	"fmt"
	"sync"
)

// Element represents an element in a stack.
type Element struct {
	Value interface{}
	next  *Element
}

// Stack data structure
type Stack struct {
	top  *Element
	size int
	mx   sync.Mutex
}

// Push pushes a value onto the stack.
func (s *Stack) Push(v interface{}) *Element {
	e := &Element{
		Value: v,
	}
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.top != nil {
		e.next = s.top
	}
	s.top = e
	s.size++
	return s.top
}

// Pop removes an element from the stack.
func (s *Stack) Pop() *Element {
	s.mx.Lock()
	defer s.mx.Unlock()
	e := s.top
	if s.top != nil {
		s.size--
		s.top = s.top.next
	}
	return e
}

// Peek peeks at the topmost element in the stack.
func (s *Stack) Peek() *Element {
	s.mx.Lock()
	defer s.mx.Unlock()
	return s.top
}

// Empty returns true is the stack is empty.
func (s *Stack) Empty() bool {
	s.mx.Lock()
	defer s.mx.Unlock()
	return (s.top == nil)
}

// Size returns the size of the stack.
func (s *Stack) Size() int {
	s.mx.Lock()
	defer s.mx.Unlock()
	return s.size
}

// Print prints the contents of the stack.
func (s *Stack) Print() {
	s.mx.Lock()
	defer s.mx.Unlock()

	for i := s.top; i != nil; i = i.next {
		fmt.Printf("%v ", i.Value)
	}
	fmt.Println()
}
