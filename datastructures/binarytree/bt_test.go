package binarytree

import (
	"fmt"
	"testing"
)

func TestBinaryTree(t *testing.T) {
	bt := &BinaryTree{}

	bt.Insert(&TreeElement{Value: 1}, nil)
	bt.Insert(&TreeElement{Value: 2}, 1)
	bt.Insert(&TreeElement{Value: 3}, 1)
	bt.Insert(&TreeElement{Value: 4}, 2)
	bt.Insert(&TreeElement{Value: 5}, 2)
	bt.Insert(&TreeElement{Value: 6}, 3)

	ep := func(e *TreeElement) {
		fmt.Printf("%v ", e.Value)
	}
	bt.PreOrder(ep)
	bt.InOrder(ep)
	bt.PostOrder(ep)
	bt.ProcessLevelByLevel(ep)
}
