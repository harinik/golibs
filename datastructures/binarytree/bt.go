// Package binarytree contains an implementation of a binary tree
// Note: the implementation is not thread-safe.
package binarytree

import (
	"fmt"

	"gitlab.com/harinik/datastructures/go/queue"
)

// TreeElement represents an element in the binary tree.
type TreeElement struct {
	Value       interface{}
	left, right *TreeElement
}

// BinaryTree data structure.
type BinaryTree struct {
	root *TreeElement
}

// Find finds an element with the specified value in the binary tree.
func (t *BinaryTree) Find(val interface{}) *TreeElement {
	return findHelper(t.root, val)
}

func findHelper(root *TreeElement, val interface{}) *TreeElement {
	if root == nil {
		return nil
	}
	if root.Value == val {
		return root
	}

	e := findHelper(root.left, val)
	if e == nil {
		e = findHelper(root.right, val)
	}
	return e
}

// Insert inserts an element into the binary tree under the parent with a specified value.
func (t *BinaryTree) Insert(child *TreeElement, parentVal interface{}) (*TreeElement, error) {
	if t.root == nil {
		t.root = child
		return t.root, nil
	}
	e := t.Find(parentVal)
	if e == nil {
		return nil, fmt.Errorf("parent value does not exist. cannot insert child")
	}
	if e.left == nil {
		e.left = child
		return child, nil
	}
	if e.right == nil {
		e.right = child
		return child, nil
	}
	return nil, fmt.Errorf("parent already has children. cannot insert child")
}

// ProcessLevelByLevel processes the tree level by level, applying
// the specified function to each element.
func (t *BinaryTree) ProcessLevelByLevel(fn func(*TreeElement)) {
	q := &queue.Queue{}

	if t.root == nil {
		return
	}

	q.Enqueue(t.root)
	for !q.Empty() {
		sz := q.Size()
		for i := 0; i < sz; i++ {
			e := q.Dequeue().Value.(*TreeElement)
			fn(e)
			if e.left != nil {
				q.Enqueue(e.left)
			}
			if e.right != nil {
				q.Enqueue(e.right)
			}
		}
		fmt.Println()
	}
}

// PreOrder traversal of the binary tree.
func (t *BinaryTree) PreOrder(fn func(*TreeElement)) {
	preOrderHelper(t.root, fn)
	fmt.Println()
}

func preOrderHelper(root *TreeElement, fn func(*TreeElement)) {
	if root != nil {
		fn(root)
		if root.left != nil {
			preOrderHelper(root.left, fn)
		}
		if root.right != nil {
			preOrderHelper(root.right, fn)
		}
	}
}

// InOrder traversal of the binary tree.
func (t *BinaryTree) InOrder(fn func(*TreeElement)) {
	inOrderHelper(t.root, fn)
	fmt.Println()
}

func inOrderHelper(root *TreeElement, fn func(*TreeElement)) {
	if root != nil {
		if root.left != nil {
			inOrderHelper(root.left, fn)
		}
		fn(root)
		if root.right != nil {
			inOrderHelper(root.right, fn)
		}
	}
}

// PostOrder traversal of the binary tree.
func (t *BinaryTree) PostOrder(fn func(*TreeElement)) {
	postOrderHelper(t.root, fn)
	fmt.Println()
}

func postOrderHelper(root *TreeElement, fn func(*TreeElement)) {
	if root != nil {
		if root.left != nil {
			postOrderHelper(root.left, fn)
		}
		if root.right != nil {
			postOrderHelper(root.right, fn)
		}
		fn(root)
	}
}
