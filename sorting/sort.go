package sort

// Element interface that is comparable
type Element interface {
	Compare(e Element) int
}

// InsertionSort implements insertion sort on an array of comparable elements.
func InsertionSort(arr []Element) {
	for i := 1; i < len(arr); i++ {
		j := i
		for ; j > 0 && arr[j-1].Compare(arr[j]) > 0; j-- {
			arr[j-1], arr[j] = arr[j], arr[j-1]
		}
	}
}

// SelectionSort implements selection sort on an array of comparable elements.
func SelectionSort(arr []Element) {
	for i := 0; i < len(arr); i++ {
		min := i
		for j := i + 1; j < len(arr); j++ {
			if arr[j].Compare(arr[min]) < 0 {
				min = j
			}
		}
		if min != i {
			arr[i], arr[min] = arr[min], arr[i]
		}
	}
}

// MergeSort implements merge sort on an array of comparable elements.
func MergeSort(arr []Element) {
	work := make([]Element, len(arr))

	mergeSortHelper(arr, work, 0, len(arr))
}

func mergeSortHelper(arr, work []Element, start, end int) {

	if (end - start) == 1 {
		return
	}
	mid := (start + end) / 2

	mergeSortHelper(arr, work, start, mid)
	mergeSortHelper(arr, work, mid, end)
	merge(arr, work, start, mid, end)
	// copy the working slice back into arr
	for i := start; i < end; i++ {
		arr[i] = work[i]
	}
}

func merge(arr, work []Element, start, mid, end int) {
	i := start
	j := mid

	k := start
	for ; i < mid && j < end; k++ {
		if arr[i].Compare(arr[j]) < 0 {
			work[k] = arr[i]
			i++
		} else {
			work[k] = arr[j]
			j++
		}
	}
	if i < mid {
		for ; i < mid; i++ {
			work[k] = arr[i]
			k++
		}
	}
	if j < end {
		for ; j < end; j++ {
			work[k] = arr[j]
			k++
		}
	}
}

// QuickSort implements quick sort on an array of comparable elements.
func QuickSort(arr []Element) {
	quickSortHelper(arr, 0, len(arr))
}

func quickSortHelper(arr []Element, start, end int) {
	if start < end {
		p := partition(arr, start, end)
		quickSortHelper(arr, start, p)
		quickSortHelper(arr, p+1, end)
	}
}

func partition(arr []Element, start, end int) int {
	pivot := arr[end-1]
	i := start
	for j := start; j < end-1; j++ {
		if arr[j].Compare(pivot) < 0 {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	arr[i], arr[end-1] = arr[end-1], arr[i]
	return i
}
