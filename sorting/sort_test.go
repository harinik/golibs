package sort

import (
	"testing"
)

// Int is a simple wrapper around int that implements Compare.
type Int struct {
	val int
}

func (ae Int) Compare(e Element) int {
	ae2 := e.(Int)
	rc := 0
	if ae.val > ae2.val {
		rc = 1
	} else if ae.val < ae2.val {
		rc = -1
	}
	return rc
}

func Equals(a, b []Element) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		if a[i].Compare(b[i]) != 0 {
			return false
		}
	}
	return true
}

type TestCase struct {
	name            string
	input, expected []Element
	sortfn          func([]Element)
}

func TestSorting(t *testing.T) {
	expected := []Element{
		Int{1}, Int{2}, Int{3}, Int{4}, Int{5},
	}

	tests := []TestCase{
		{
			name: "InsertionSort",
			input: []Element{
				Int{1}, Int{4}, Int{5}, Int{2}, Int{3},
			},
			expected: expected,
			sortfn:   InsertionSort,
		},
		{
			name: "SelectionSort",
			input: []Element{
				Int{1}, Int{4}, Int{5}, Int{2}, Int{3},
			},
			expected: expected,
			sortfn:   SelectionSort,
		},
		{
			name: "MergeSort",
			input: []Element{
				Int{1}, Int{4}, Int{5}, Int{2}, Int{3},
			},
			expected: expected,
			sortfn:   MergeSort,
		},
		{
			name: "QuickSort",
			input: []Element{
				Int{1}, Int{4}, Int{5}, Int{2}, Int{3},
			},
			expected: expected,
			sortfn:   QuickSort,
		},
	}

	for _, test := range tests {
		test.sortfn(test.input)
		if !Equals(test.input, test.expected) {
			t.Errorf("test: %v expected: %v got: %v", test.name, test.expected, test.input)
		}
	}
}
